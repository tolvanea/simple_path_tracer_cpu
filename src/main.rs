use rayon::prelude::*;                      // Multi-threading
use image::{save_buffer, ColorType::Rgb8};  // Saving PNG-image
use glam::{Vec2, Vec3, Vec4, vec2, vec3, vec4, Vec2Swizzles, Vec3Swizzles, Vec4Swizzles}; // f32-vec
use indicatif::ProgressBar;
use std::{f32::consts::PI, sync::Mutex, time::Instant};
type F = f32;   // used to call math functions with like F::sin(x) rather than x.sin()

// QUALITY SETTING:
// Comment and uncomment some of the following lines to change the quality of image.
const RES: [usize; 2] = [640, 360]; const RAYS: usize = 20; const BOUNCES: usize = 2;    // 30 sec
//const RES: [usize; 2] = [960, 540]; const RAYS: usize = 400; const BOUNCES: usize = 5;   // 1 h
//const RES: [usize; 2] = [1920, 1080]; const RAYS: usize = 4000; const BOUNCES: usize = 5; // 2 day

/// Pseudorandom number generator taking influence from fxhash (firefox hash)
struct Rnd (usize);
impl Rnd {
    fn f(&mut self) -> f32 {  // Generates a real on range [0,1]
        self.0 = (self.0.rotate_left(5) ^ 12345).wrapping_mul(0x517cc1b727220a95);
        return self.0 as f32 / usize::MAX as f32;
    }
}

struct SurfaceInfo{ color: Vec3, shininess: f32, escaped: bool, is_light: bool, metallic: bool}

fn main() {
    let mut img = [[[0_u8; 3]; RES[0]]; RES[1]];

    // Time measurement and progress bar
    let (now, bar) = (Instant::now(), Mutex::new(ProgressBar::new(RES[1] as u64)));

    // Render to byte array 'img', rows are processed parallelly in different threads.
    img.par_iter_mut().enumerate()
        .for_each(|(y_idx, row)| {
            for (x_idx, pixel) in row.iter_mut().enumerate() {
                *pixel = path_trace(x_idx, y_idx);  // Render image pixel-by-pixel
            }
            bar.try_lock().map(|bar| bar.inc(1)).ok();  // Increment progress bar
        }
    );

    bar.lock().unwrap().finish();
    println!("Elapsed {} sec", now.elapsed().as_secs_f32());

    // Write byte array to png-file.
    let buffer: &[u8; RES[0]*RES[1]*3] = unsafe{std::mem::transmute(&img)};
    save_buffer("out138_add_metal.png", buffer, RES[0] as u32, RES[1] as u32, Rgb8).unwrap()
}

/// The magic happens here: Taking x and y coordinate of an image, return the color of that pixel.
fn path_trace(x_ixd: usize, y_ixd: usize) -> [u8; 3] {
    let mut pixel = Vec3::ZERO;           // Color of pixel as Vec3<f32>
    for ray_idx in 0..RAYS {
        let mut rnd = Rnd((y_ixd*RES[0] + x_ixd)*RAYS + ray_idx);  // Random number generator + seed
        let mut mask: Vec3 = Vec3::ONE;   // Accumulated absorption for a ray after surface bounces

        let (mut dir, mut origin) = shoot_ray_from_camera(x_ixd, y_ixd, &mut rnd);  // Initial ray
        for _ in 0..BOUNCES {
            let (distance, surface_data) = ray_intersection(origin, dir);  // calculate collision
            let surf = surface_info(dir, distance, surface_data);

            if surf.is_light { pixel += mask * surf.color; break; } // Only light sources give color
            if surf.escaped { break; }  // Ray escaped the scene, start again with a new ray

            // The rest of this loop body bounces the ray and updates parameters for next iteration

            origin += dir * distance;   // Update ray origin to the point of hit
            let normal = calc_normal(origin);
            // Fresnel coefficient with Schlick's approximation, using glass refractive index n=1.5.
            // Did you know that glass reflects as much light as any other material with same n?
            let reflected = rnd.f() > 0.04 + 0.96 * (1.0 - normal.dot(-dir).max(0.0)).powi(5);
            dir = if reflected {  // the probability of reflection from n=1.5 surface
                specular_scattering(dir, normal, surf.shininess, &mut rnd)  // Glossy surface
            } else {
                diffuse_scattering(normal, &mut rnd)  // Rough surface
            };
            if reflected || surf.metallic {
                mask *= surf.color;  // Light is absorbed only if not reflected from surface
            }
        }
    }
    // PNG uses SRGB color space, which requires gamma correction. gamma = linear^2.2
    let srgb_pixel = (pixel / RAYS as f32).clamp(Vec3::ZERO, Vec3::ONE).powf(0.4545);  // l^(1/2.2)
    return (srgb_pixel*256.0).as_ref().map(|v| v as u8);
}

/// Shoot ray for a given pixel. Includes anti aliasing, depth of field, and bloom (=lens blur).
fn shoot_ray_from_camera(x_idx: usize, y_idx: usize, rnd: &mut Rnd) -> (Vec3, Vec3) {
    let fov = PI / 5.0;           // vertical field of view in radians
    let aperture_radius = 0.005;  // depth of field or "bokeh"
    let focus_distance = 0.8;

    // Pick random point on circular aperture
    let (sint, cost) = F::sin_cos(rnd.f() * 2.0 * PI);
    let lens_point: Vec3 = aperture_radius * F::sqrt(rnd.f()) * vec3(sint, cost, 0.0);

    // Pick a point on focus plane, which is pixel grid in front of camera. Account anti aliasing
    // Using right handed coordinates: x is right, y is up, and camera is looking at -z
    let hr = 0.5 * vec2(RES[0] as f32, RES[1] as f32);  // half resolution
    let screen_coord = vec2(x_idx as f32 - hr.x + rnd.f(), hr.y - y_idx as f32 + rnd.f());
    let focus_point = (screen_coord * F::tan(fov / 2.0) / hr.y).extend(-1.0) * focus_distance;

    // Shoot a ray from aperture to focus plane
    let ray_dir = (focus_point - lens_point).normalize();

    // Add bloom/blur. I found that Gaussian blur can be calculated with Phong scattering cos^n(θ).
    // As n goes infinity, Phong becomes gaussian distribution N(0, σ), with n = 1/tan(σ)^2.
    let sigma = fov / (5000.0 * rnd.f().powi(2)*9.0 + 10.0);  // σ is mostly negligibly narrow
    let ray_dir = specular_scattering(-ray_dir, ray_dir, 1.0/(F::tan(sigma).powi(2)), rnd);

    // You can change camera position here. Sorry, no camera rotation, it would take few extra lines
    let position = vec3(0.2, 0.5, 1.4) + lens_point;
    return (ray_dir, position);
}

/// Fetch information from ray collision.
fn surface_info(dir: Vec3, distance: f32, surface_data: Vec3) -> SurfaceInfo {
    if distance < 0.0 {  // Ray escapes to infinity, so it hits either the sun or the sky
        let color = match vec3(1.0, 1.0, 0.5).normalize().dot(dir) > 0.99 {  // Sky color
            true  => vec3(1.0,0.93,0.88) * 50.0,  // Sun, color temperature T=5500K
            false => vec3(0.78,0.84,1.0) * (0.4 + 0.4*dir.y) * (0.4 + 0.4*dir.x), // Sky gradient
        };
        return SurfaceInfo{ color, shininess: 0.0, escaped: true, is_light: true, metallic: false};
    }
    let grey = vec4(0.2, 0.2, 0.2, 2.0);  // Background color, slight gradient
    // The `cs` means "color and shininess", which is vec4(red, green, blue, shininess).
    let (cs, is_light, metallic) = match surface_data {
        s if s.y.log2()/32.0 + s.x > 1.3
            => (vec4(1.0, 0.2, 0.3, 0.0) * 13.0, true, false),  // red light on tips of buds
        s if (6.0*s.x + 0.37).fract() > 0.9
            => (vec4(0.8, 0.4, 0.9, 40.0), false, true),        // stripes of violet metallic paint
        s if s.y > 2.5 => {  // Blending between 3 surfaces: matte grey, glossy green and chrome
            let (green,  is_g) = (vec4(0.2, 0.8, 0.6, 12.0), (s.y.sqrt()/5.0-0.4).clamp(0.0, 1.0));
            let (orange, is_o) = (vec4(0.9, 0.9, 0.3, 80.0), (s.y.log2()-7.0).clamp(0.0, 1.0));
            (grey.lerp(green, is_g).lerp(orange, is_o), false, false)  // linear interpolation
        },
        s if (12.7*s.z - 0.1).abs() > 0.02 && (4.7*s.z - 0.03).abs() < 0.02
            => (vec4(0.2, 0.5, 1.0, 0.0) * 16.0,  true, false),  // cyan light stripes
        _ => (grey, false, false),
    };
    return SurfaceInfo{ color: cs.xyz(), shininess: cs.w, escaped: false, is_light, metallic};
}

/// Calculate distance for ray to hit the object. Use ray marching: Get spehere radius that is known
/// to be empty, and move ray forward by that safe distance. Continue until sphere gets very small.
fn ray_intersection(ray_origin: Vec3, ray_dir: Vec3) -> (f32, Vec3) {
    let mut t: f32 = 0.0;    // Travelled distance on line
    let mut h = Vec4::ZERO;  // Nearest distance to the object (x) + iteration information (y,z,w)
    for _ in 0..512 {
        h = map(ray_origin + ray_dir*t);
        if t > 10.0 || h.x < 0.001*t { break; }  // Ray escapes or hits surface
        t += h.x;
    }
    return (if t<10.0 {t} else {-1.0}, h.yzw());  // Distance -1 means that nothing was hit.
}

/// Distance estimate of a 3d-fractal. This gives the geometry and the surface patterns.
/// Input: point in 3d-space. Output: distance to the object, and arbitrary iteration information
/// This is alternative version of commonly known "mandelbulb" (, which is broken near the poles.)
/// http://iquilezles.org/www/articles/mandelbulb/mandelbulb.htm
/// https://www.shadertoy.com/view/ltfSWn
fn map(p: Vec3) -> Vec4 {
    const DEG: f32 = 5.0;       // Degree of mandelbulb.
    let mut w: Vec3 = p.yzx();  // This coordinate is rotated around and around in iteration
    let mut r = w.length();
    let mut dr: f32 = 1.0;      // Derivative of the steepest gradient
    let mut s = vec3(r, r, w.x.abs());  // for surface color and light
    for _ in 0..5 {
        dr = DEG * r.powi(DEG as i32 - 1)*dr + 1.0;
        let (sa, ca) = F::sin_cos(DEG*F::acos(w.y / r));
        let (sb, cb) = F::sin_cos(DEG*F::atan2(w.x, w.z));
        w = p + r.powi(DEG as i32) * vec3(sb*sa, cb, sb*ca);
        r = w.length();
        s = vec3(s.x.min(r), s.y.max(r), s.z.min(w.x.abs()));
        if r > 16.0 {break;}
    }
    return vec4(0.5*F::ln(r)*r/dr, s.x, s.y, s.z);
}

/// Generate a new direction according to ideal Lambert scattering, where scattering is unaffected
/// from incoming light direction. The probability function is a cosine of angle between the
/// scattered direction and the normal. See: http://www.amietia.com/lambertnotangent.html
/// This is the same as calling `specular_scattering(-normal, normal, 1.0, rnd)`. The algorithm is:
/// 1. Generate a random point on unit sphere using Archimedes hat-box theorem.
/// 2. Place the sphere tangent to surface, and take normalized direction with respect to origin.
fn diffuse_scattering(normal: Vec3, rnd: &mut Rnd) -> Vec3 {
    let ((sin_phi, cos_phi), z) = (F::sin_cos(2.0*PI * rnd.f()), 2.0*rnd.f()-1.0);
    let w = F::sqrt(1.0-z*z);  // Equivalent form is w = sin(theta) = sin(acos(z))
    return (normal + vec3(cos_phi*w, sin_phi*w, z)).normalize();  // vec3 is a point on unit sphere
}

/// Generate a new direction according to Phong scattering, which applies for glossy surfaces.
/// The probability function is cos^n of the angle between scatter direction and the reflection
/// direction. See more: http://www.igorsklyar.com/system/documents/papers/4/fiscourse.comp.pdf
fn specular_scattering(dir: Vec3, normal: Vec3, n: f32, rnd: &mut Rnd) -> Vec3 {
    // Pick the new direction in coordinates where incoming is aligned with z'-axis (marked z2)
    let z2 = rnd.f().powf(1.0/(1.0+n));  // n is measure of shininess
    let (sin_p, cos_p) = F::sin_cos(2.0*PI*rnd.f());
    let w = F::sqrt(1.0 - z2.powi(2));  // now x' = cos_p*w and y' = sin_p*w
    // Rotate this direction so that z-axis is around reflection direction of the normal
    let reflect = |dir: Vec3| dir - 2.0*dir.dot(normal) * normal;
    let z: Vec3 = reflect(dir);  // Reflection named as "z" because it is z'-axis in the above basis
    let (x, y) = make_orthonormal_basis(z);  // Vectors x, y, z are orthonormal
    let new_dir = x*cos_p*w + y*sin_p*w + z*z2;   // Rotate the generated direction
    let is_outwards = normal.dot(new_dir) >= 0.0; // If new direction points inwards, reflect
    return if is_outwards {new_dir} else {reflect(new_dir)}
}

/// Generate two vectors that are orthonormal to the given one. Commonly used to rotate vectors.
/// Highly efficient algorigthm invented in 2017: https://jcgt.org/published/0006/01/01/paper.pdf
fn make_orthonormal_basis(n: Vec3) -> (Vec3, Vec3) {
    let s = (1.0_f32).copysign(n.z);  // sign
    let a = -1.0 / (s + n.z);
    let b = n.x*n.y*a;
    return (vec3(1.0 + s*n.x.powi(2)*a, s*b, -s*n.x), vec3(b, s + n.y.powi(2)*a, -n.y));
}

/// Calculate normal: https://iquilezles.org/www/articles/normalsSDF/normalsSDF.htm
fn calc_normal (p: Vec3) -> Vec3 {
    let e = Vec2::new(1.0,-1.0) * 0.00001;
    return (  // This clever hack requires only 4 evaluation points (tetrahedra), rather than 3*2.
        e.xyy()*map(p + e.xyy()).x + e.yyx()*map(p + e.yyx()).x +
        e.yxy()*map(p + e.yxy()).x + e.xxx()*map(p + e.xxx()).x
    ).normalize();
}
