# Simple path tracer (CPU version)

Simple and naive CPU-based path tracer implemented within 230 lines of rust code. Outputs a png-image. Depending on quality setting found from beginning of `src/main.rs`, rendering takes from 30 seconds to 2 days.

![fractal](screenshots/132.png)


## What is path tracer?
If you do not know much about 3d graphics, the _path tracer_ is basically same as ray tracer, but it just casts more rays with more bounces, which renders better images. Everything is based on fairly simple equation known as _rendering equation_, which tells how illuminance on a point of a surface is given as a sum of all lights shining to that point. The light is simulated with millions of rays bouncing from surfaces. This program does not utilize performance optimizations known as _importance sampling_, so the bounce direction of light ray is realistic rather than smart. This program demonstrates that 3d-rendering is not magic and it runs on quite basic principles.

Where in real life light rays are emitted by light and captured by camera, path tracer does the reverse by shooting light rays from camera and capturing them with lights. The reverse works, because the surface scattering is symmetrical in both directions.

Path tracer in a nutshell:
1. For each pixel in image, shoot a ray from camera pointing to that direction.
2. Calculate intersection with the environment, that is, obtain the surface.
    * If the surface is a light source, assign it as the color of the ray and continue from step 1.
    * Else, if maximal number of bounces is exceeded, make the ray black and continue from step 1.
    * Else, bounce the ray in some random direction depending on the glossiness of the surface. Also, accumulate the absorption of the ray by color of the surface. Then, continue from step 2.
4. After shooting enough rays per pixel, evaluate the color of the pixel as the average of the rays.

Check out the actual logic from function `path_trace`. In a nutshell, it takes x and y coordinates of the image and outputs the color for that pixel.

## Where did all the geometry come from?
The geometry is a variation of a [mandelbulb fractal](https://www.shadertoy.com/view/ltfSWn). The way ray-intersection is calculated with geometry is called _ray marching_. It is based on a function (`map()`) which takes in a point of space and tells how far is the nearest surface from that point. The rays are marched forward within spheres that are known to not contain any surfaces. This is continued until ray gets really close to surface, which will yield us intersection point.

## CPU vs GPU
I have implemented almost identical path tracer, which uses GPU computation instead:

https://gitlab.com/tolvanea/simple_path_tracer/-/tree/1.0

The main differences is that the GPU version
* is 10-100 times faster
* is written in WLSL (WebGPU shading language)
* contains 1000 lines of boilerplate code (rust) to initialize GPU and window
* allows you to interactively change camera position and lens settings
* has a version of `path_trace` function that is optimized for lock-step nature of GPU computation. The code is slightly less obvious and the upper limit of ray bounces differs a bit.

## Resources
This work was greatly inspired by the work of iq and fizzer. For example, see
* https://www.shadertoy.com/view/MsdGzl
* http://iquilezles.org/www/articles/simplepathtracing/simplepathtracing.htm
* https://www.shadertoy.com/view/ll3SDr


## Licence
Do the whatever you want.
